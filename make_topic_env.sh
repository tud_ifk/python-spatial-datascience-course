#!/bin/sh
module load Anaconda3
conda config --set channel_priority strict
cd /home/$USER
mkdir topic_env_user
conda env create \
    --prefix /home/$USER/topic_env_user/topic_env_user \
    --file /home/$USER/mobile_cart_workshop2020.git/environment_topics.yml \
    --quiet
source /sw/installed/Anaconda3/2019.03/bin/activate /home/$USER/topic_env_user/topic_env_user
conda install ipykernel --channel conda-forge
/home/$USER/topic_env_user/topic_env_user/bin/python \
    -m ipykernel install \
    --user \
    --name topic_env_user \
    --display-name="topic_env_user"
